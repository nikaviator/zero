#include <vector>
#ifndef ROBOT_MODELS_WO_H
#define ROBOT_MODELS_WO_H
#include "quaternion.h"
using namespace std;
class worldobject
{
public:
     vector<float> p1;
     std::string name;
     double x,y,z,w;
     // constructor
     worldobject() : x{}, y{}, z{}, w{} {
         p1.resize(3);
     }
};
#endif //ROBOT_MODELS_TEST_H