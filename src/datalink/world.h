#include "humanspace.h"
#include "abstractobject.h"
#include "robot.h"
#ifndef ROBOT_MODELS_W_H
#define ROBOT_MODELS_W_H
class world
{
public:
    humanspace *humans;
    vector<abstractobject*> objects;
    // we cannot construct the robot here, because we do not know if
    // we want to construct a regular robot or a robotfpe
    robot *c;

    /*humanspace *a=new humanspace;
    abstractobject *b1=new abstractobject;
    abstractobject *b2=new abstractobject;
    robot *c=new robot;*/
};
#endif //ROBOT_MODELS_TEST_H
