#include <ros/ros.h>
#include "datalink/abstractobject.h"
#include "datalink/graspobject.h"
#include "datalink/hand.h"
#include "datalink/humanspace.h"
#include "datalink/joint.h"
#include "datalink/obstacle.h"
#include "datalink/robot.h"
#include "datalink/robotfpe.h"
#include "datalink/world.h"
#include "datalink/worldobject.h"
#include "datalink/quaternion.h"
#include <vector>
#include <iostream>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <cstdio>
#include <tf2/LinearMath/Quaternion.h>
#include "tf2_ros/message_filter.h"
#include "message_filters/subscriber.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "geometry_msgs/PointStamped.h"
#include <tf2_ros/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/trajectory_processing/iterative_time_parameterization.h>
#include "trajectory_msgs/JointTrajectoryPoint.h"

int main(int argc, char **argv) {
    ros::init(argc, argv, "WM_TEST_NODE");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ROS_INFO("WELCOME TO TEST NODE ");
    // initialize the world
    world world1;
    auto robot1 = new robotfpe;
    world1.c = robot1;
    world1.humans = new humanspace;
    world1.humans->humannum = 1;
    auto *obstacle1 = new obstacle{};
    obstacle1->name = "Cube";
    // TODO set other properties of obstacle1
    obstacle1->size = {5.2, 7.5, 9.1};
    obstacle1->p1 = {13.2, 14.1, 17.5};
    obstacle1->x = 2.3;
    obstacle1->y = 1.2;
    obstacle1->z = 0.2;
    obstacle1->w = 0.1;
    world1.objects.push_back(obstacle1);
    auto *graspobject1 = new graspobject;
    // TODO set the properties of the grasp object
    graspobject1->name = "Ball";
    graspobject1->size = {1.3, 2.1, 3.2};
    graspobject1->p1 = {12.5, 16.4, 17.3};
    graspobject1->x = 1.1;
    graspobject1->y = 1.4;
    graspobject1->z = 1.6;
    graspobject1->w = 1.9;
    world1.objects.push_back(graspobject1);
    // initialize the robot
    for (int i = 0; i < 7; i++) {
        robot1->jo.emplace_back();
        // we want the positions of the joints, but have the position of the links. there are 8 links, but 7 joints between
        // them. So the first joint has the position of the second link etc.
        robot1->jo[i].name = "panda_link" + std::to_string(i + 1);
    }
    robot1->h = new hand{};
    // TODO set properties of robot1->h
    robot1->h->grippos = {12.1, 14.3, 18.2};
    // TODO set other properties of robot1
    robot1->of = true;
    robot1->moving = false;
    robot1->grasppos = true;
    robot1->planninggrp = "Panda Teaching";
    // TODO test if the robot contains 7 joints
    if (robot1->jo.size() == 7) { ROS_INFO_STREAM(" TEST CASE SUCCESSFUL. ROBOT FPE HAS 7 JOINTS "); }
    else { ROS_ERROR_STREAM(" TEST CASE FAILED. ROBOT FPE HAS WRONG NUMBER OF JOINTS = " << robot1->jo.size()); }
    // TODO test if the world contains two objects
    if (world1.objects.size() == 2) { ROS_INFO_STREAM(" TEST CASE SUCCESSFUL. WORLD CONTAINS TWO OBJECTS "); }
    else { ROS_ERROR_STREAM(" TEST CASE FAILED. WORLD HAS WRONG NUMBER OF OBJECTS = " << world1.objects.size()); }
    // TODO perform nullptr checks
    if (robot1->h == nullptr)
        ROS_ERROR("ERROR IN HAND POINTER IN ROBOT FPE CLASS AGGREGATION RELATION");
    if (world1.humans == nullptr)
        ROS_ERROR("ERROR IN POINTER WORLD HUMANSPACE AGGREGATION");
    if (world1.c == nullptr)
        ROS_ERROR("ERROR IN ROBOT CLASS AGGREGATION");
    if (world1.objects[0] == nullptr)
        ROS_ERROR("ERROR IN ABSTRACT OBJECT CLASS AGGREGATION RELATION");
    if (world1.objects[1] == nullptr)
        ROS_ERROR("ERROR IN ABSTRACT OBJECT CLASS AGGREGATION RELATION");
    // TODO set another_pose
    // TODO move the robot to another_pose
    static const std::string PLANNING_GROUP = "panda_arm";
    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    ros::Duration(3.0).sleep();
    move_group.setStartStateToCurrentState();

    geometry_msgs::Pose another_pose;
    another_pose.orientation.w = 1.0;
    another_pose.position.x = 0.4;
    another_pose.position.y = -0.4;
    another_pose.position.z = 0.9;
    move_group.setPoseTarget(another_pose);

    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
    ROS_INFO("Visualizing plan %s", success ? "" : "FAILED");
    // Move the (simulated) robot in gazebo
    move_group.move();
    //ros::shutdown();
    // End Robot Motion
    // Start input values from Gazebo
    // ros::Duration(3.0).sleep();
    // geometry_msgs::Pose another_pose;
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);
    ros::Rate rate(10);
    while (node_handle.ok()) {
        rate.sleep(); // if we sleep at the beginning of the loop,
        // the first lookup also succeeds
        // at this point we have checked the size of jo, so we can assume
        // it is correct, thus we can iterate over jo
        for (auto &i : robot1->jo) {
            try {
                auto transformStamped = tfBuffer.lookupTransform("world", i.name, ros::Time(0));
                i.p1[0] = transformStamped.transform.translation.x;
                // TODO set other parts of pose
                i.p1[1] = transformStamped.transform.translation.y;
                i.p1[2] = transformStamped.transform.translation.z;
            } catch (tf2::TransformException &ex) {
                ROS_WARN("%s", ex.what());
                ros::Duration(0.1).sleep();
            }
        }
        // dynamic tests
        // test x and y position
        double epsilon = 0.001; // 1mm deviation is allowed
        double dev_x = 0; // TODO compute dev_x/y using std::abs, the position in
        double dev_y = 0; //      the model and another_pose.position.x/y
        dev_x=std::abs(robot1->jo[6].p1[0]-another_pose.position.x);
        dev_y=std::abs(robot1->jo[6].p1[1]-another_pose.position.y);

        if (dev_x<=epsilon&&dev_y<=epsilon) { // TODO add correct condition
            ROS_INFO_STREAM(" ROBOT MOVED TO INTENDED POSITION. x/y deviations are " << dev_x << "/" << dev_y);
            break; // the test succeeded, we can exit the loop
        } else {
            ROS_ERROR_STREAM("ROBOT DID NOT MOVE TO INTENDED POSITION. x/y deviations are " << dev_x << "/" << dev_y);
        }
    }
    return 0;
}
/*
abstractobject o1;
graspobject o2;
hand o3;
humanspace o4;
joint o5a;
joint o5b;
joint o5c;
joint o5d;
joint o5e;
joint o5f;
joint o5g;
obstacle o6;
robot robot1;
robotfpe o8;
world o9;
worldobject o10;

robot1.jo.push_back(o5a);
robot1.jo.push_back(o5b);
robot1.jo.push_back(o5c);
robot1.jo.push_back(o5d);
robot1.jo.push_back(o5e);
robot1.jo.push_back(o5f);
robot1.jo.push_back(o5g);

int yx = 0;
robot1.jo[++yx].name = "panda_link1";
robot1.jo[++yx].name = "panda_link2";
robot1.jo[++yx].name = "panda_link3";
robot1.jo[++yx].name = "panda_link4";
robot1.jo[++yx].name = "panda_link5";
robot1.jo[++yx].name = "panda_link6";
robot1.jo[++yx].name = "panda_link7";

o9.c->robname = "Robot FPE";
o9.c->of = true;
o9.c->moving = false;
o9.c->grasppos = true;
o9.c->planninggrp = "Panda Teaching";

o8.h->grippos = {12.1, 14.3, 18.2};
o3.grippos = {1.1, 2.3, 2.1};

o9.a->humannum = 1;
o9.b1->name = "Cube";
o9.b1->size = {5.2, 7.5, 9.1};
o9.b1->p1 = {13.2, 14.1, 17.5};
o9.b1->x = 2.3;
o9.b1->y = 1.2;
o9.b1->z = 0.2;
o9.b1->w = 0.1;
o9.b2->name = "Ball";
o9.b2->size = {1.3, 2.1, 3.2};
o9.b2->p1 = {12.5, 16.4, 17.3};
o9.b2->x = 1.1;
o9.b2->y = 1.4;
o9.b2->z = 1.6;
o9.b2->w = 1.9;

// Static Tests - World Model Safety Test Cases to check Segmentation error
// test the properties of the model that do not change over time, such as the structure of you model
if (robot1.jo.size() == 7) { ROS_INFO_STREAM(" TEST CASE SUCCESSFUL. ROBOT FPE HAS 7 JOINTS "); }
else { ROS_ERROR_STREAM(" TEST CASE FAILED. ROBOT FPE HAS WRONG NUMBER OF JOINTS = " << robot1.jo.size()); }

if (o8.h == nullptr)
    ROS_ERROR("ERROR IN HAND POINTER IN ROBOT FPE CLASS AGGREGATION RELATION");

if (o9.a == nullptr)
    ROS_ERROR("ERROR IN POINTER WORLD HUMANSPACE AGGREGATION");

if (o9.c == nullptr)
    ROS_ERROR("ERROR IN ROBOT CLASS AGGREGATION");

if (o9.b1 == nullptr)
    ROS_ERROR("ERROR IN ABSTRACT OBJECT CLASS AGGREGATION RELATION");

if (o9.b2 == nullptr)
    ROS_ERROR("ERROR IN ABSTRACT OBJECT CLASS AGGREGATION RELATION");
// End of static tests
// Start Robot Motion
static const std::string PLANNING_GROUP = "panda_arm";
moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
ros::Duration(3.0).sleep();
move_group.setStartStateToCurrentState();

geometry_msgs::Pose another_pose;
another_pose.orientation.w = 1.0;
another_pose.position.x = 0.4;
another_pose.position.y = -0.4;
another_pose.position.z = 0.9;
move_group.setPoseTarget(another_pose);

moveit::planning_interface::MoveGroupInterface::Plan my_plan;
bool success = (move_group.plan(my_plan) == moveit::planning_interface::MoveItErrorCode::SUCCESS);
ROS_INFO("Visualizing plan %s", success ? "" : "FAILED");
// Move the (simulated) robot in gazebo
move_group.move();
//ros::shutdown();

// End Robot Motion
// Start input values from Gazebo to program
tf2_ros::Buffer tfBuffer;
tf2_ros::TransformListener tfListener(tfBuffer);
ros::Rate rate(10);

while (node_handle.ok()) {
    geometry_msgs::TransformStamped transformStamped1;
    geometry_msgs::TransformStamped transformStamped2;
    geometry_msgs::TransformStamped transformStamped3;
    geometry_msgs::TransformStamped transformStamped4;
    geometry_msgs::TransformStamped transformStamped5;
    geometry_msgs::TransformStamped transformStamped6;
    geometry_msgs::TransformStamped transformStamped7;
    try {
        transformStamped1 = tfBuffer.lookupTransform("world", robot1.jo[1].name, ros::Time(0));
        transformStamped2 = tfBuffer.lookupTransform("world", robot1.jo[2].name, ros::Time(0));
        transformStamped3 = tfBuffer.lookupTransform("world", robot1.jo[3].name, ros::Time(0));
        transformStamped4 = tfBuffer.lookupTransform("world", robot1.jo[4].name, ros::Time(0));
        transformStamped5 = tfBuffer.lookupTransform("world", robot1.jo[5].name, ros::Time(0));
        transformStamped6 = tfBuffer.lookupTransform("world", robot1.jo[6].name, ros::Time(0));
        transformStamped7 = tfBuffer.lookupTransform("world", robot1.jo[7].name, ros::Time(0));
        robot1.jo[7].p1[2] = transformStamped7.transform.translation.z;
        if (robot1.jo[7].p1[2] == 0.9)
            ROS_INFO_STREAM(" THE ROBOT HAS MOVED TO POSITION AS INTENDED ");
        else
            ROS_ERROR_STREAM("THE ROBOT HAS NOT MOVED TO THE POSITION AS INTENDED");
        rate.sleep();
        break;
    } catch (tf2::TransformException &ex) {
        ROS_WARN("%s", ex.what());
        ros::Duration(0.1).sleep();
        continue;
    }
}
// Dynamic Tests
while (node_handle.ok()) {
    {// update the model data like in the application model node, test the dynamic parts here, i.e., if the model data is consistent with the position of the robot
        //Checking if robot moved to position or not
        if (robot1.jo[7].p1[2] == 0.9)
            ROS_INFO_STREAM(" THE ROBOT HAS MOVED TO POSITION AS INTENDED ");
        else
            ROS_ERROR_STREAM("THE ROBOT HAS NOT MOVED TO THE POSITION AS INTENDED");
        rate.sleep();
    }

}
return 0;
}*/
